TARGET_FPU = "hard"

require conf/machine/include/tune-ppce5500.inc
require conf/machine/include/soc-family.inc

MACHINEOVERRIDES =. "e5500:"

require conf/machine/include/qoriq-ppc.inc
