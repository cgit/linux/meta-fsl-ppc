DESCRIPTION = "Userspace debug agent for PA CodeWarrior"
LICENSE = "Freescale-EULA"
LIC_FILES_CHKSUM = "file://EULA;md5=c9ae442cf1f9dd6c13dfad64b0ffe73f"

DEPENDS = "elfutils"

inherit kernel-arch

SRC_URI = "git://git.freescale.com/ppc/sdk/apptrk.git;branch=sdk-v2.0.x"
SRCREV = "873f44ca6b219508f738825299453d92975fb897"

S = "${WORKDIR}/git"

EXTRA_OEMAKE += "ARCH=${ARCH}"
CFLAGS_append = " -I${STAGING_INCDIR} -ISource/Linux -ISource/Portable"
CFLAGS_append_qoriq-ppc = " -ISource/Linux_PA -ISource/PA -DPPC"
CFLAGS_append_powerpc64 = " -DENABLE_64BIT_SUPPORT"

do_install() {
        install -d ${D}/usr/bin
        oe_runmake install DESTDIR=${D}
}

INSANE_SKIP_${PN} = "already-stripped"
COMPATIBLE_MACHINE = "(qoriq-ppc)"
