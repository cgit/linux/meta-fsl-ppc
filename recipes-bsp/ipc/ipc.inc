DEPENDS = "virtual/kernel"

SRC_URI = "git://git.freescale.com/ppc/sdk/ipc.git;branch=sdk-v2.0.x \
    file://Makefile-use-LDFLAGS-if-set.patch \
"
SRCREV = "74d662707558290f070f9589177db730444bc435"

COMPATIBLE_MACHINE = "(bsc9132qds|bsc9131rdb|b4860qds|b4420qds)"
